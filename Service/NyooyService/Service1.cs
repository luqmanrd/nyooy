﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace NyooyService
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer tmrExecutor = new System.Timers.Timer();
        db_nyooyEntities _context = new db_nyooyEntities();
        int ScheduleTime = Convert.ToInt32(ConfigurationSettings.AppSettings["ThreadTime"]);
        public Thread Worker = null;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                tmrExecutor.Elapsed += new ElapsedEventHandler(tmrExecutor_Elapsed);
                tmrExecutor.Interval = 10000;
                tmrExecutor.Enabled = true;
                tmrExecutor.Start();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void tmrExecutor_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            var db_order = _context.tbl_order.Where(o => o.order_status == "Need to Pay").ToList();
            foreach (var item in db_order)
            {
                DateTime order_date = item.order_date.Value;
                DateTime maxTimeToPay = order_date.AddMinutes(30);
                DateTime now = DateTime.Now;
                if (DateTime.Compare(now, maxTimeToPay) > 0)
                {
                    using (var save = _context.Database.BeginTransaction())
                    {
                        try
                        {
                            var order = _context.tbl_order.Where(o => o.id == item.id).FirstOrDefault();
                            order.order_status = "Cancelled";
                            _context.SaveChanges();
                            save.Commit();
                            //Send email order_status
                            var dbUser = _context.tbl_user.Where(u => u.id == order.id_user).SingleOrDefault();

                            //Declare variable
                            var emailFrom = "nyooy.id@gmail.com"; //set Email Address FROM
                            var password = "GOFOOD & GRAB FOOD"; //set Password
                            var emailTo = dbUser.email; //set Email Address TO

                            var subject = "Order Cancellation";

                            MailMessage mail = new MailMessage();
                            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                            mail.From = new MailAddress(emailFrom);
                            mail.To.Add(emailTo);
                            mail.Subject = subject;
                            //mail.Body = "This is for testing SMTP mail from GMAIL";
                            mail.Body = ContentBody(order.id);
                            mail.IsBodyHtml = true;

                            SmtpServer.Port = 587; //-> for Gmail use port 587 
                            SmtpServer.Credentials = new System.Net.NetworkCredential(emailFrom, password);
                            SmtpServer.EnableSsl = true;
                            SmtpServer.Send(mail);
                        }
                        catch(Exception error)
                        {
                            save.Rollback();
                            db_nyooyEntities _contextError = new db_nyooyEntities();
                            tbl_log_history_error err = new tbl_log_history_error
                            {
                                user_name = "Nyooy Service",
                                error_message = error.Message,
                                error_position = "Check Payment Service",
                                error_date = DateTime.Now,
                                is_deleted = 0,
                            };

                            _contextError.tbl_log_history_error.Add(err);
                            _contextError.SaveChanges();
                        }
                    }
                }
                
            }
            Thread.Sleep(ScheduleTime * 10 * 1000);
            //string path = "C:\\sample.txt";
            //using (StreamWriter writer = new StreamWriter(path, true))
            //{
            //    writer.WriteLine(string.Format("Windows Service is Called on " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
            //    Thread.Sleep(ScheduleTime * 60 * 1000);
            //}
        }

        protected override void OnStop()
        {
            try
            {
                tmrExecutor.Enabled = false;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string ContentBody(long orderID)
        {
            var bodyHtml = @"<html>
<body>
	<table border='0' cellpadding='0' cellspacing='0' width='100%'>
		<!-- LOGO -->
		<tr>
			<td bgcolor='#FFA73B' width='100%' align='center' style='padding: 40px 10px 40px 10px;'></td>
		</tr>
		<tr>
			<td bgcolor='#FFA73B' align='center' style='padding: 0px 10px 0px 10px;'>
				<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>
					<tr>
						<td bgcolor='#ffffff' align='center' valign='top' style='padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;'>
							<h1 style='font-size: 48px; font-weight: 400; margin: 2;'>Order Cancelled</h1> 
							<img src='https://img.icons8.com/fluent/96/000000/sad.png' width='125' height='120' style='display: block; border: 0px;' />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td bgcolor='#f4f4f4' align='center' style='padding: 0px 10px 0px 10px;'>
				<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>
					<tr>
						<td bgcolor='#ffffff' align='center' style='padding: 20px 30px 40px 30px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>
							<p style='margin: 0;'>Hello customer we're really sorry, but we've canceled your Order (ID: "+ orderID +@" ) because we didn't recieve any payment slip for your order.</p>
						</td>
					</tr>
					<tr>
						<td bgcolor='#ffffff' align='center' style='padding: 0px 30px 20px 30px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>
							<p style='margin: 0;'>If you have any questions, just reply to this email—we're always happy to help out.</p>
						</td>
					</tr>
					<tr>
						<td bgcolor='#ffffff' align='right' style='padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>
							<p style='margin: 0;'>Cheers,<br><br><br>Nyooys Team</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td bgcolor='#f4f4f4' align='center' style='padding: 0px 10px 40px 10px;'>
				<table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>
					<tr>
						<td bgcolor='#FFECD1' align='center' style='padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>
							<h2 style='font-size: 20px; font-weight: 400; color: #111111; margin: 0;'>Need more help?</h2>
							<p style='margin: 0;'>
								<a href='https://nyooy.id/home/about' target='_blank' style='color: #FFA73B;'>We&rsquo;re here to help you out</a>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>";

            return bodyHtml;
        }
    }
}
