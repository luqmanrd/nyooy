use db_nyooy
GO

--Insert tbl_user
--User
INSERT INTO tbl_user VALUES ('Super Admin', 08123456789, 'Jl. Sulfat Agung III No. 12', Convert(datetime, '01-01-2001', 103), 'super.admin@gmail.com', 'fWgd31BX3ZI=','super_admin', 0, null, Convert(datetime, '01-10-2020', 103), 1)
GO

INSERT INTO tbl_user VALUES ('Admin', 08123456789, 'Jl. Sulfat Agung III No. 12', Convert(datetime, '01-01-2001', 103), 'admin@gmail.com', 'fWgd31BX3ZI=','admin', 0, null, Convert(datetime, '01-10-2020', 103), 1)
GO

INSERT INTO tbl_user VALUES ('Ilham Rizaldi', 08123456789, 'Jl. Soekarno Hatta No 1', Convert(datetime, '01-01-2001', 103), 'user@gmail.com', 'fWgd31BX3ZI=','user', 0, null, Convert(datetime, '01-11-2020', 103), 1)
GO

--Insert tbl_product
--Maem'e
INSERT INTO tbl_product VALUES (1, 'Sambal Matah', 'Maem''e', 15000, 0, 'Sambal Matah.jpg', 'Menu makanan sambal matah', 0)
GO

INSERT INTO tbl_product VALUES (1, 'Teriyakuy', 'Maem''e', 16000, 0, 'Teriyakuy.jpg', 'Menu makanan Teriyakuy', 0)
GO

INSERT INTO tbl_product VALUES (1, 'Chicken Nyooy Black Butter', 'Maem''e', 17000, 0, 'Chicken Nyooy Black Butter.jpg', 'Menu makanan Chicken Nyooy Black Butter', 0)
GO

INSERT INTO tbl_product VALUES (1, 'Chicken Nyooy Original', 'Maem''e', 14000, 0, 'Chicken Nyooy Original.jpg', 'Menu makanan Chicken Nyooy Original', 0)
GO

--Promo
INSERT INTO tbl_product VALUES (1, 'Package A', 'Promo', 25000, 0, 'Package A.JPG', 'Menu makanan Package A', 15000)
GO

INSERT INTO tbl_product VALUES (1, 'Package B', 'Promo', 30000, 0, 'Package B.JPG', 'Menu makanan Package B', 20000)
GO

--Jajan'e
INSERT INTO tbl_product VALUES (1, 'Snack Bucket', 'Jajan''e', 15000, 0, 'Snack Bucket.jpg', 'Menu makanan Snack Bucket', 0)
GO

--Mimik'e
INSERT INTO tbl_product VALUES (1, 'Pinky Berry', 'Mimik''e', 15000, 0, 'Pinky Berry.jpg', 'Menu minuman Pinky Berry', 0)
GO


--Insert Payment Method
INSERT INTO tbl_payment VALUES ('Transfer (Manual Check)', 0)
GO

INSERT INTO tbl_payment VALUES ('Barcode', 0)
GO

--Insert Order
INSERT INTO tbl_order VALUES (3, 2, 'Success', Convert(datetime, '16-10-2020', 103), 0, 'Personal', NULL, NULL, NULL, 2, 'Jl.Pondok Pinang', 10000, NULL)
GO

--Insert Detail Order
INSERT INTO tbl_detail_order VALUES (1, 5, 15000, 0, 1)
GO

INSERT INTO tbl_detail_order VALUES (2, 2, 10000, 0, 1)
GO

INSERT INTO tbl_detail_order VALUES (3, 3, 12000, 0, 1)
GO

-- Insert Review
INSERT INTO tbl_review VALUES (1, 'SO DELICIOUS', Convert(datetime, '18-10-2020', 103), 0, 1)
GO

--Insert Config
INSERT INTO tbl_config Values (10000)
GO