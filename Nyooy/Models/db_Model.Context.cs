﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nyooy.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class db_nyooyEntities : DbContext
    {
        public db_nyooyEntities()
            : base("name=db_nyooyEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<tbl_courier> tbl_courier { get; set; }
        public virtual DbSet<tbl_detail_order> tbl_detail_order { get; set; }
        public virtual DbSet<tbl_history_order> tbl_history_order { get; set; }
        public virtual DbSet<tbl_history_product> tbl_history_product { get; set; }
        public virtual DbSet<tbl_log_history_error> tbl_log_history_error { get; set; }
        public virtual DbSet<tbl_order> tbl_order { get; set; }
        public virtual DbSet<tbl_payment> tbl_payment { get; set; }
        public virtual DbSet<tbl_product> tbl_product { get; set; }
        public virtual DbSet<tbl_user> tbl_user { get; set; }
        public virtual DbSet<tbl_user_activation> tbl_user_activation { get; set; }
        public virtual DbSet<tbl_review> tbl_review { get; set; }
        public virtual DbSet<tbl_config> tbl_config { get; set; }
    }
}
