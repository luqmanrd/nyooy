﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nyooy.Models
{
    [MetadataType(typeof(tbl_product_MetaData))]

    public class tbl_product_MetaData

    {

        [Display(Name = "Product's Name")]
        public string name { get; set; }

        [Display(Name = "Category")]
        public string category { get; set; }

        [Display(Name = "Price")]
        public Nullable<int> price { get; set; }

        public Nullable<int> is_deleted { get; set; }

        [Display(Name = "Photo")]
        public string photo { get; set; }

        [Display(Name = "Description")]
        public string description { get; set; }

        [Display(Name = "Promo")]
        public Nullable<int> promo { get; set; }

    }
}