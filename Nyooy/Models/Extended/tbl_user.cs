﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Nyooy.Models
{
    [MetadataType(typeof(tbl_user_MetaData))]

    public partial class tbl_user

    {
        [NotMapped]//Exclude from database
        [Display(Name = "Old Password")]
        public string oldPassword { get; set; }

        [NotMapped]//Exclude from database
        [Display(Name = "Confirm Password")]
        public string confirmPassword { get; set; }

    }

    public class tbl_user_MetaData

    {

        [Display(Name = "Name")]
        public string name { get; set; }

        [Display(Name = "Phone")]
        public string no_hp { get; set; }

        [Display(Name = "Address")]
        public string address { get; set; }

        [Display(Name = "Date of Birth")]
        public Nullable<System.DateTime> dob { get; set; }

        [Display(Name = "Email")]
        public string email { get; set; }

        [Display(Name = "Password")]
        public string password { get; set; }

        [NotMapped]//Exclude from database
        [Display(Name = "Confirm Password")]
        public string confirmPassword { get; set; }

        [NotMapped]//Exclude from database
        [Display(Name = "Old Password")]
        public string oldPassword { get; set; }

        [Display(Name = "Photo")]
        public string foto { get; set; }

    }
}