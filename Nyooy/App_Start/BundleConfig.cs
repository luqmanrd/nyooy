﻿using System.Web;
using System.Web.Optimization;

namespace Nyooy
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/logincss").Include(
                      "~/Content/styles/bootstrap.min.css",
                      "~/Content/styles/shards-dashboards.1.1.0.css",
                      "~/Content/styles/custom.css",
                      "~/Content/DataTables/datatables.min.css",
                      "~/Content/DataTables/DataTables-1.10.22/css/jquery.dataTables.min.css"));

            bundles.Add(new StyleBundle("~/Content/admincss").Include(
                      "~/Content/styles/all.css",
                      "~/Content/styles/bootstrap.min.css",
                      "~/Content/styles/shards-dashboards.1.1.0.css",
                      "~/Content/styles/custom.css",
                      "~/Content/styles/extras.1.1.0.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/adminjs").Include(
                      "~/Content/scripts/button.js",
                      "~/Content/scripts/jquery-3.3.1.min.js",
                      "~/Content/scripts/popper.min.js",
                      "~/Content/scripts/bootstrap.min.js",
                      "~/Content/scripts/Chart.min.js",
                      "~/Content/scripts/shards.min.js",
                      "~/Content/scripts/jquery.sharrre.min.js",
                      "~/Content/scripts/extras.1.1.0.min.js",
                      "~/Content/scripts/shards-dashboards.1.1.0.min.js",
                      "~/Content/scripts/app/app-blog-overview.1.1.0.js",
                      "~/Content/DataTables/datatables.min.js",
                      "~/Content/DataTables/DataTables-1.10.22/js/jquery.dataTables.min.js"));

            bundles.Add(new StyleBundle("~/Content/maincss").Include(
                      "~/Content/styles/all.css",
                      "~/Content/main/css/bootstrap.min.css",
                      "~/Content/main/css/plugins.css",
                      "~/Content/main/css/style.css",
                      "~/Content/main/css/plugins/material-design-iconic-font.min.css",
                      "~/Content/main/css/plugins/owl.carousel.min.css",
                      "~/custom.css"));

            bundles.Add(new ScriptBundle("~/bundles/modernizrjs").Include(
                      "~/Content/main/js/vendor/modernizr-3.5.0.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/mainjs").Include(
                      "~/Content/main/js/vendor/jquery-3.2.1.min.js",
                      "~/Content/main/js/popper.min.js",
                      "~/Content/main/js/bootstrap.min.js",
                      "~/Content/main/js/plugins.js",
                      "~/Content/main/js/active.js"));
        }
    }
}
