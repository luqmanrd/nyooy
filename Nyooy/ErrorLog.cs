﻿using Nyooy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nyooy
{
    public class ErrorLog
    {
        private db_nyooyEntities _contextError;

        public ErrorLog()
        {
            _contextError = new db_nyooyEntities();
        }

        public void Save(string name, string errorMessage, string position)
        {
            tbl_log_history_error err = new tbl_log_history_error
            {
                user_name = name,
                error_message = errorMessage,
                error_position = position,
                error_date = DateTime.Now,
                is_deleted = 0,
            };

            _contextError.tbl_log_history_error.Add(err);
            _contextError.SaveChanges();
        }
    }
}