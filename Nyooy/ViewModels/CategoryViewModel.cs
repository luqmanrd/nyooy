﻿using Nyooy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nyooy.ViewModels
{
    public class CategoryViewModel
    {
        public tbl_product Product { get; set; }
        public IEnumerable<string> Category { get; set; }
    }

    public class CartViewModel
    {
        public long id_product { get; set; }
        public string name { get; set; }
        public int qty { get; set; }
        public string photo { get; set; }
        public Nullable<int> price { get; set; }
        public Nullable<int> total_price { get; set; }
    }
    
    public class YourOrderViewModel
    {
        public long id { get; set; }
        public string order_status { get; set; }
        public Nullable<System.DateTime> order_date { get; set; }
        public string order_category { get; set; }
        public Nullable<int> total_price { get; set; }
    }

    public class CourierViewModel
    {
        public IEnumerable<tbl_order> Order { get; set; }
        public IEnumerable<tbl_user> User { get; set; }
        public IEnumerable<tbl_courier> Courier { get; set; }
    }

    public class DetailViewModel
    {
        public string name { get; set; }
        public Nullable<int> qty { get; set; }
        public Nullable<int> total_price { get; set; }
        public tbl_review Review { get; set; }
    }

    public class StatusViewModel
    {
        public tbl_order Order { get; set; }
        public IEnumerable<tbl_user> Courier { get; set; }
        public IEnumerable<DetailViewModel> detail { get; set; }
        public IEnumerable<string> order_status { get; set; }
        public long id_courier { get; set; }
    }

    public class OrderListViewModel
    {
        public IEnumerable<OrderViewModel> Order { get; set; }
    }

    public class OrderViewModel
    {
        public long id { get; set; }
        public string order_status { get; set; }
        public Nullable<System.DateTime> order_date { get; set; }
        public string user_name { get; set; }
        public string order_category { get; set; }
        public Nullable<int> total_price { get; set; }
    }

    public class OrdersDetailViewModel
    {
        public OrderDetailViewModel Order { get; set; }
        public IEnumerable<tbl_user> Courier { get; set; }
        public IEnumerable<DetailViewModel> detail { get; set; }
        public IEnumerable<string> order_status { get; set; }
        public long id_courier { get; set; }
    }
    public class OrderDetailViewModel
    {
        public long id { get; set; }
        public Nullable<long> id_courier { get; set; }
        public long id_payment { get; set; }
        public string order_status { get; set; }
        public Nullable<System.DateTime> order_date { get; set; }
        public string order_category { get; set; }
        public string event_name { get; set; }
        public string organizer { get; set; }
        public string order_payment { get; set; }
        public string delivery_address { get; set; }
        public Nullable<int> delivery_fee { get; set; }
        public string payment_name { get; set; }
        public string user_name { get; set; }
        public string order_note { get; set; }
    }
    public class ReviewViewModel
    {
        public IEnumerable<tbl_order> Order { get; set; }
        public IEnumerable<tbl_user> User { get; set; }
        public IEnumerable<tbl_review> Review { get; set; }
    }

    public class DashboardViewModel
    {
        public int productSale { get; set; }
        public int countProduct { get; set; }
        public int countReviews { get; set; }
        public int countUsers { get; set; }
        public int countStaff { get; set; }
    }

    public class TrackViewModel
    {
        public IEnumerable<tbl_courier> track { get; set; }
    }

    public class TrackHistoryProduct
    {
        public IEnumerable<tbl_user> User { get; set; }
        public IEnumerable<tbl_product> Product { get; set; }
        public IEnumerable<tbl_history_product> HistoryProduct { get; set; }
    }

    public class TrackingHistoryCourier
    {
        public IEnumerable<tbl_user> User { get; set; }
        public IEnumerable<tbl_courier> Courier { get; set; }
        public IEnumerable<tbl_order> Order { get; set; }
    }

    public class HistoryOrder
    {
        public long id { get; set; }
        public long id_order { get; set; }
        public string username { get; set; }
        public string status_order { get; set; }
        public Nullable<System.DateTime> update_date { get; set; }
    }

    public class DetailDelivery
    {
        public string name { get; set; }
        public string no_hp { get; set; }
        public string delivery_address { get; set; }
    }
    public class DetailProductDelivery
    {
        public string product_name { get; set; }
        public int? qty { get; set; }
    }
    public class DetailCourier
    {
        public tbl_courier Courier { get; set; }
        public IEnumerable<DetailProductDelivery> Product { get; set; }
        public DetailDelivery Delivery { get; set; }
    }
}
