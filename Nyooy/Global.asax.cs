﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Nyooy
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
    public class AdminSessionCheck : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            if (session["email"] == null)
            {
                filterContext.Result = new RedirectResult("~/login");
            }
            else
            {
                if (session["role"].ToString() == "user")
                {
                    filterContext.Result = new RedirectResult("~/");
                }
            }
            
        }
    }
    public class SuperAdminSessionCheck : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            if (session["email"] == null)
            {
                filterContext.Result = new RedirectResult("~/login");
            }
            else
            {
                if (session["role"].ToString() != "super_admin")
                {
                    filterContext.Result = new RedirectResult("~/Admin");
                }
            }

        }
    }
    public class SessionCheck : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            if (session["email"] != null)
            {
                if (session["role"].ToString() == "admin" || session["role"].ToString() == "super_admin")
                {
                    filterContext.Result = new RedirectResult("~/Admin");
                }
                else
                {
                    filterContext.Result = new RedirectResult("~/");
                }
            }
        }
    }
    public class LoginCheck : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            if (session["email"] == null)
            {
                filterContext.Result = new RedirectResult("~/Login");
            }
        }
    }
}
