﻿using Newtonsoft.Json;
using Nyooy.Models;
using Nyooy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Nyooy.Controllers
{
    [AdminSessionCheck]
    public class OrdersController : Controller
    {
        private db_nyooyEntities _context;
        private ErrorLog _err;

        public OrdersController()
        {
            _context = new db_nyooyEntities();
            _err = new ErrorLog();
        }

        // GET: Orders
        public ActionResult Index()
        {
            //var db = _context.tbl_order.Where(p => p.is_deleted == 0).OrderByDescending(p => p.order_date).ToList();
            var db = (from d in _context.tbl_detail_order
                      join o in _context.tbl_order on d.id_order equals o.id
                      join u in _context.tbl_user on o.id_user equals u.id
                      group d by new { o.id, o.order_category, o.order_date, o.order_status, o.delivery_fee, u.name } into g
                      select new OrderViewModel
                      {
                          id = g.Key.id,
                          order_category = g.Key.order_category,
                          order_date = g.Key.order_date,
                          order_status = g.Key.order_status,
                          user_name = g.Key.name,
                          total_price = g.Sum(d => d.total_price) + g.Key.delivery_fee
                      }).OrderByDescending(o => o.order_date).ToList();
            if(TempData["Success"] != null)
            {
                ViewBag.Success = TempData["Success"].ToString();
                TempData.Remove("Success");
            }
            if (TempData["Error"] != null)
            {
                ViewBag.Error = TempData["Error"].ToString();
                TempData.Remove("Error");
            }
            return View(db);
        }

        public ActionResult Details(int id)
        {
            try
            {
                var db = (from o in _context.tbl_order
                          join p in _context.tbl_payment on o.id_payment equals p.id
                          join u in _context.tbl_user on o.id_user equals u.id
                          where o.id == id
                          select new OrderDetailViewModel
                          {
                              id = o.id,
                              order_category = o.order_category,
                              order_date = o.order_date,
                              delivery_address = o.delivery_address,
                              delivery_fee = o.delivery_fee,
                              order_payment = o.order_payment,
                              order_status = o.order_status,
                              event_name = o.event_name,
                              id_courier = o.id_courier,
                              id_payment = o.id_payment,
                              organizer = o.organizer,
                              payment_name = p.name,
                              user_name = u.name,
                              order_note = o.order_note
                          }).FirstOrDefault();
                var order_detail = (from d in _context.tbl_detail_order
                                    join p in _context.tbl_product on d.id_product equals p.id
                                    where d.id_order == id
                                    select new DetailViewModel
                                    {
                                        name = p.name,
                                        qty = d.qty,
                                        total_price = d.total_price
                                    }).ToList();
                var courier = _context.tbl_user.Where(c => c.role == "admin" || c.role == "super_admin").ToList();

                long total = 0;
                foreach (var item in order_detail)
                {
                    total += Convert.ToInt64(item.total_price);
                }
                ViewBag.total_price = total;
                var status = new List<string>
                        {
                            "Need to Pay",
                            "Wait for Payment Confirmation",
                            "On Process",
                            "On Delivery",
                            "Success",
                            "Cancelled"
                        };

                var viewModel = new OrdersDetailViewModel
                {
                    Order = db,
                    Courier = courier,
                    detail = order_detail,
                    order_status = status
                };
                if (TempData["Success"] != null)
                {
                    ViewBag.Success = TempData["Success"].ToString();
                    TempData.Remove("Success");
                }
                if (TempData["Error"] != null)
                {
                    ViewBag.Error = TempData["Error"].ToString();
                    TempData.Remove("Error");
                }
                if(db == null)
                {
                    TempData["Error"] = "Data not Found!";
                    return this.RedirectToAction("Index", "Orders");
                }
                else
                {
                    return View(viewModel);
                }
            }
            catch
            {
                TempData["Error"] = "Data not Found!";
                return this.RedirectToAction("Index", "Orders");
            }
        }

        [HttpPost]
        public ActionResult Save(tbl_order order)
        {
            long user_id = (long) Session["id"];
            dynamic name = Session["name"];

            using (var save = _context.Database.BeginTransaction())
            {
                try
                {
                    var db_update_order = _context.tbl_order.Where(p => p.id == order.id).SingleOrDefault();
                    db_update_order.order_status = order.order_status;
                    if (order.order_status.ToString() == "On Delivery")
                    {
                        db_update_order.id_courier = order.id_courier;
                    }

                    //Send email order_status
                    var dbUser = _context.tbl_user.Where(u => u.id == db_update_order.id_user).SingleOrDefault();

                    //Declare variable
                    var emailFrom = "nyooy.id@gmail.com"; //set Email Address FROM
                    var password = "GOFOOD & GRAB FOOD"; //set Password
                    var emailTo = dbUser.email; //set Email Address TO

                    var subject = "Your Status Order";
                    var title = "Nyooy.id";
                    var content = @"Customer Notification: “Your order status is <strong>" + db_update_order.order_status + "</strong> Now! Let's check your order in our app”";
                    var footer = "Thank your for your order!";

                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                    mail.From = new MailAddress(emailFrom);
                    mail.To.Add(emailTo);
                    mail.Subject = subject;
                    //mail.Body = "This is for testing SMTP mail from GMAIL";
                    mail.Body = BodyHtml(title, content, footer);
                    mail.IsBodyHtml = true;

                    SmtpServer.Port = 587; //-> for Gmail use port 587 
                    SmtpServer.Credentials = new System.Net.NetworkCredential(emailFrom, password);
                    SmtpServer.EnableSsl = true;


                    _context.SaveChanges();
                    save.Commit();
                    TempData["Success"] = "Order Data Successfully Updated!";
                    SmtpServer.Send(mail);
                    
                    return this.RedirectToAction("Details", "Orders", new { id = order.id });
                }
                catch (Exception e)
                {
                    _err.Save("visitor", e.Message, "Update Status Order");
                    ViewBag.Message = e.Message;
                    save.Rollback();
                    TempData["Error"] = "Order Data Failed to Update!";
                    return this.RedirectToAction("Details", "Orders", new { id = order.id });
                }
            }
        }

        public string BodyHtml(string title, string bodyContent, string footer)
        {
            var bodyHtml = @"<html>" +
            "<body>" +
            "<table bgcolor='#f5feff'  align='center' border='0' cellpadding='0' cellspacing='0' width='80%' style='border: none;'>" +
                "<tr>" +
                    "<td align='center' style='padding: 0 0 0 0;'>" +
                        "<img src='https://miro.medium.com/max/712/1*c3cQvYJrVezv_Az0CoDcbA.jpeg' width='100%' alt='Notification' style='display: block;' />" +
                    "</td>" +
                "</tr>" +
                "<tr>" +
                    "<td style='padding: 40px 30px 40px 30px;'>" +
                        "<table border='0' cellpadding='0' cellspacing='0' width='100%'>" +
                            "<tr style='text-align:center; color: #153643; font-family: Arial, sans-serif; font-size: 24px;'>" +
                                "<td>" +
                                    "<strong>" + title + "</strong>" +
                                "</td>" +
                            "</tr>" +
                            "<tr style='color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;'>" +
                                "<td style='padding: 20px 0 30px 0; text-align: justify;'>" +
                                    bodyContent +
                                "</td>" +
                            "</tr>" +
                        "</table>" +
                    "</td>" +
                "</tr>" +
                "<tr style='text-align:center'>" +
                    "<td bgcolor='#ffe05d' style='padding: 30px 30px 30px 30px;'>" +
                        footer +
                    "</td>" +
                "</tr>" +
                "</table>" +
                "</body>" +
                "</html>";

            return bodyHtml;
        }
    }
}