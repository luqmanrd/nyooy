﻿using Nyooy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nyooy.Controllers
{
    public class MenuController : Controller
    {
        private db_nyooyEntities _context;

        public MenuController()
        {
            _context = new db_nyooyEntities();
        }

        // GET: Menu
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Category(string id)
        {
            if (id == "maem")
            {
                var db = _context.tbl_product.Where(p => p.category == "Maem'e").Where(p => p.is_deleted == 0).ToList();
                ViewBag.Category = "Maem'e";
                return View(db);
            }
            else if (id == "jajan")
            {
                var db = _context.tbl_product.Where(p => p.category == "Jajan'e").Where(p => p.is_deleted == 0).ToList();
                ViewBag.Category = "Jajan'e";
                return View(db);
            }
            else if (id == "mimik")
            {
                var db = _context.tbl_product.Where(p => p.category == "Mimik'e").Where(p => p.is_deleted == 0).ToList();
                ViewBag.Category = "Mimik'e";
                return View(db);
            }
            else
            {
                return RedirectToAction("Menu", "Home");
            }
        }

        public ActionResult Details(int id)
        {
            try
            {
                var db = _context.tbl_product.Where(p => p.id == id).SingleOrDefault();
                return View(db);
            }
            catch (Exception)
            {

                return RedirectToAction("Menu", "Home");
            }
        }
    }
}