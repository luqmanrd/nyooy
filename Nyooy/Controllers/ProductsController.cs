﻿using Nyooy.Models;
using Nyooy.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nyooy.Controllers
{
    [AdminSessionCheck]
    public class ProductsController : Controller
    {
        // GET: Products
        private db_nyooyEntities _context;
        private ErrorLog _err;

        public ProductsController()
        {
            _context = new db_nyooyEntities();
            _err = new ErrorLog();
        }

        public ActionResult Index()
        {
            var db = _context.tbl_product.Where(p => p.is_deleted == 0).ToList();
            if (TempData["Success"] != null)
            {
                ViewBag.Success = TempData["Success"].ToString();
                TempData.Remove("Success");
            }
            if (TempData["Error"] != null)
            {
                ViewBag.Error = TempData["Error"].ToString();
                TempData.Remove("Error");
            }
            return View(db);
        }

        public ActionResult Create()
        {
            var category = new List<string>
            {
                "Maem'e",
                "Jajan'e",
                "Mimik'e",
                "Promo"
            };

            var viewModel = new CategoryViewModel
            {
                Category = category
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Save(tbl_product product, HttpPostedFileBase files)
        {

            dynamic user_id = Session["id"];
            dynamic name = Session["name"];

            using (var save = _context.Database.BeginTransaction())
            {
                try
                {
                    if (product.name == null || product.category == null || product.price == null)
                    {
                        ViewBag.Message = "Another Field is empty!";

                        var category = new List<string>
                        {
                            "Maem'e",
                            "Jajan'e",
                            "Mimik'e",
                            "Promo"
                        };

                        var viewModel = new CategoryViewModel
                        {
                            Category = category
                        };

                        if (product.id == 0)
                        {
                            return View("Create", viewModel);
                        }
                        else
                        {
                            var id = Convert.ToString(product.id);
                            var encryptId = Convert.ToString(EncryptDecrypt.Base64Encode(id));

                            Details(encryptId);
                        }

                    }

                    var dbProduct = _context.tbl_product.Where(p => p.name == product.name).SingleOrDefault();

                    if (dbProduct != null)
                    {
                        ViewBag.Message = "Product Already Exist";

                        var category = new List<string>
                        {
                            "Maem'e",
                            "Jajan'e",
                            "Mimik'e",
                            "Promo"
                        };

                        var viewModel = new CategoryViewModel
                        {
                            Category = category
                        };

                        if (product.id == 0)
                        {
                            return View("Create", viewModel);
                        }
                        else
                        {
                            var id = Convert.ToString(product.id);
                            var encryptId = Convert.ToString(EncryptDecrypt.Base64Encode(id));

                            TempData["Failed"] = ViewBag.Message;

                            return RedirectToAction("Details", "Products", new { url =  encryptId });
                        }
                    }

                    //Location Path Image
                    if (files != null)
                    {
                        Random random = new Random();
                        var ext = System.IO.Path.GetExtension(files.FileName);
                        var file_name_as_product = product.name + random.Next() + ext;
                        //var fileName = Path.GetFileName(files.FileName);
                        string path = Path.Combine(Server.MapPath("~/Images/product/"), file_name_as_product);
                        product.photo = file_name_as_product;
                        files.SaveAs(path);
                    }

                    if (product.promo == null)
                    {
                        product.promo = 0;
                    }

                    if (product.description == null)
                    {
                        product.description = "";
                    }

                    //save to tbl_history_product
                    tbl_history_product _historyProduct = new tbl_history_product();

                    if (product.id == 0)
                    {
                        product.id_user = user_id;
                        product.is_deleted = 0;
                        _context.tbl_product.Add(product);

                        _historyProduct.status_product = "Create";
                    }
                    else
                    {
                        var db_edit_product = _context.tbl_product.Where(p => p.id == product.id).SingleOrDefault();

                        db_edit_product.id_user = user_id;
                        db_edit_product.name = product.name;
                        db_edit_product.category = product.category;
                        db_edit_product.price = product.price;
                        db_edit_product.description = product.description;
                        db_edit_product.promo = product.promo;

                        if (files != null)
                        {
                            db_edit_product.photo = product.photo;
                        }


                        _historyProduct.status_product = "Edit";

                    }


                    _historyProduct.id_product = product.id;
                    _historyProduct.id_user = user_id;
                    _historyProduct.update_date = DateTime.Now;
                    _context.tbl_history_product.Add(_historyProduct);
                    //save to tbl_history_product


                    _context.SaveChanges();
                    save.Commit();
                    var db = _context.tbl_product.Where(p => p.is_deleted == 0).ToList();
                    ViewBag.Success = "Product Data Successfully Saved";
                    return View("Index", db);

                }
                catch (Exception e)
                {
                    _err.Save(name, e.Message, "Submit Form Product");
                    save.Rollback();
                    var db = _context.tbl_product.Where(p => p.is_deleted == 0).ToList();
                    ViewBag.Error = e.Message;
                    return View("Index", db);
                }

            }
        }

        public ActionResult Details(string url)
        {

            try
            {

                if (TempData["Failed"] != null)
                {
                    ViewBag.Message = TempData["Failed"];

                    TempData.Remove("Failed");
                }

                var id = Convert.ToInt32(EncryptDecrypt.Base64Decode(url));

                var db = _context.tbl_product.Where(p => p.id == id).SingleOrDefault();

                var category = new List<string>
                        {
                            "Maem'e",
                            "Jajan'e",
                            "Mimik'e",
                            "Promo"
                        };

                var viewModel = new CategoryViewModel
                {
                    Product = db,
                    Category = category
                };

                return View(viewModel);
            }
            catch (Exception)
            {

                var db = _context.tbl_product.Where(p => p.is_deleted == 0).ToList();
                ViewBag.Error = "Product Data Not Found!";
                return View("Index", db);
            }

        }

        public ActionResult Delete(long id)
        {
            try
            {
                dynamic user_id = Session["id"];
                var product_delete = _context.tbl_product.Where(p => p.id == id).SingleOrDefault();
                product_delete.is_deleted = 1;

                //save to tbl_history_product
                tbl_history_product _historyProduct = new tbl_history_product();
                _historyProduct.status_product = "Delete";
                _historyProduct.id_product = product_delete.id;
                _historyProduct.id_user = user_id;
                _historyProduct.update_date = DateTime.Now;
                _context.tbl_history_product.Add(_historyProduct);
                //save to tbl_history_product

                _context.SaveChanges();
                TempData["Success"] = "Product Data Successfully Deleted!";
                return RedirectToRoute(new { controller = "Products", action = "Index" });
            }
            catch
            {
                TempData["Error"] = "Failed to Delete Product Data";
                return RedirectToRoute(new { controller = "Products", action = "Index" });
            }
            
            
        }

        [HttpGet]
        public JsonResult GetData()
        {
            _context.Configuration.ProxyCreationEnabled = false;

            var dbProduct = _context.tbl_product.AsNoTracking().ToList();
            var dbHistoryProduct = _context.tbl_history_product.ToList();
            var dbUser = _context.tbl_user.AsNoTracking().Where(u => u.role == "super_admin" || u.role == "admin").ToList();

            var historyProduct = new TrackHistoryProduct
            {
                Product = dbProduct,
                HistoryProduct = dbHistoryProduct,
                User = dbUser
            };
            return Json(historyProduct, JsonRequestBehavior.AllowGet);
        }

        [SuperAdminSessionCheck]
        public ActionResult HistoryProduct()
        {
            return View();
        }

    }
}