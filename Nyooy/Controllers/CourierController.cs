﻿using Newtonsoft.Json;
using Nyooy.Models;
using Nyooy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Nyooy.Controllers
{
    [AdminSessionCheck]
    public class CourierController : Controller
    {
        private db_nyooyEntities _context;
        private db_nyooyEntities _contextError;

        public CourierController()
        {
            _context = new db_nyooyEntities();
            _contextError = new db_nyooyEntities();
        }
        // GET: Courier
        public ActionResult Index()
        {
            tbl_courier courier = new tbl_courier();
            return View(courier);
        }
        public ActionResult SendOrder(tbl_courier courier)
        {
            var dbUser = (from o in _context.tbl_order
                          join u in _context.tbl_user on o.id_user equals u.id
                          where o.id == courier.id_order
                          select new DetailDelivery
                          { 
                            name = u.name,
                            no_hp = 0+u.no_hp,
                            delivery_address = o.delivery_address,
                          }).SingleOrDefault();

            var dbProduct = (from p in _context.tbl_product
                            join d in _context.tbl_detail_order on p.id equals d.id_product
                            where d.id_order == courier.id_order
                            select new DetailProductDelivery
                            {
                                product_name = p.name,
                                qty = d.qty
                            }).ToList();


            var data = new DetailCourier
            {
                Courier = courier,
                Delivery = dbUser,
                Product = dbProduct
            };

            var settingTime = _context.tbl_config.Where(c => c.id == 1).SingleOrDefault();
            ViewBag.Time = settingTime.time_get_location;

            return View(data);
        }

        [HttpPost]
        public ActionResult Check(tbl_courier courier)
        {
            dynamic user_id = Session["id"];
            dynamic name = Session["name"];

            using (var save = _context.Database.BeginTransaction())
            {
                try
                {
                    if (courier.lat != null && courier.@long != null && courier.id_order != null)
                    {
                        //add data courier to db
                        courier.delivery_date = DateTime.Now;
                        courier.is_deleted = 0;

                        //Get Order
                        var dbOrder = _context.tbl_order.Where(o => o.id == courier.id_order).SingleOrDefault();

                        //Get user
                        var dbUser = _context.tbl_user.Where(u => u.id == dbOrder.id_user).SingleOrDefault();

                        //update data status order
                        //dbOrder.order_status = "has been received by " + dbUser.name;
                        dbOrder.order_status = "Success";

                        //Send email order_status

                        //Declare variable
                        var emailFrom = "nyooy.id@gmail.com"; //set Email Address FROM
                        var password = "GOFOOD & GRAB FOOD"; //set Password
                        var emailTo = dbUser.email; //set Email Address TO

                        var subject = "Your Status Order";
                        var title = "Nyooy.id";
                        var content = @"Customer Notification: “Your order status is <strong>" + dbOrder.order_status + "</strong> Now! Let's check your order in our app”";
                        var footer = "Thank your for your order!";

                        MailMessage mail = new MailMessage();
                        SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                        mail.From = new MailAddress(emailFrom);
                        mail.To.Add(emailTo);
                        mail.Subject = subject;
                        //mail.Body = "This is for testing SMTP mail from GMAIL";
                        mail.Body = BodyHtml(title, content, footer);
                        mail.IsBodyHtml = true;

                        SmtpServer.Port = 587; //-> for Gmail use port 587 
                        SmtpServer.Credentials = new System.Net.NetworkCredential(emailFrom, password);
                        SmtpServer.EnableSsl = true;

                        _context.tbl_courier.Add(courier);
                        _context.SaveChanges();
                        save.Commit();

                        SmtpServer.Send(mail);
                    }
                }
                catch (Exception e)
                {
                    tbl_log_history_error err = new tbl_log_history_error
                    {
                        user_name = name,
                        error_message = e.Message,
                        error_position = "Add Location Courier",
                        error_date = DateTime.Now,
                        is_deleted = 0,
                    };

                    _contextError.tbl_log_history_error.Add(err);
                    _contextError.SaveChanges();
                    save.Rollback();
                }
            }
            return RedirectToAction("Index", "Courier");
        }

        public string BodyHtml(string title, string bodyContent, string footer)
        {
            var bodyHtml = @"<html>" +
            "<body>" +
            "<table bgcolor='#f5feff'  align='center' border='0' cellpadding='0' cellspacing='0' width='80%' style='border: none;'>" +
                "<tr>" +
                    "<td align='center' style='padding: 0 0 0 0;'>" +
                        "<img src='https://miro.medium.com/max/712/1*c3cQvYJrVezv_Az0CoDcbA.jpeg' width='100%' alt='Notification' style='display: block;' />" +
                    "</td>" +
                "</tr>" +
                "<tr>" +
                    "<td style='padding: 40px 30px 40px 30px;'>" +
                        "<table border='0' cellpadding='0' cellspacing='0' width='100%'>" +
                            "<tr style='text-align:center; color: #153643; font-family: Arial, sans-serif; font-size: 24px;'>" +
                                "<td>" +
                                    "<strong>" + title + "</strong>" +
                                "</td>" +
                            "</tr>" +
                            "<tr style='color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;'>" +
                                "<td style='padding: 20px 0 30px 0; text-align: justify;'>" +
                                    bodyContent +
                                "</td>" +
                            "</tr>" +
                        "</table>" +
                    "</td>" +
                "</tr>" +
                "<tr style='text-align:center'>" +
                    "<td bgcolor='#ffe05d' style='padding: 30px 30px 30px 30px;'>" +
                        footer +
                    "</td>" +
                "</tr>" +
                "</table>" +
                "</body>" +
                "</html>";

            return bodyHtml;
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            long id_courier = (long)Session["id"];
            _context.Configuration.ProxyCreationEnabled = false;

            //NOTE : STATUS ORDER SHOULD BE 'ON DELIVERY'
            var dbOrder = _context.tbl_order.AsNoTracking().Where(o => o.order_status == "On Delivery" && o.id_courier == id_courier).ToList();
            //var dbOrder = _context.tbl_order.AsNoTracking().ToList();
            var dbUser = _context.tbl_user.Where(u => u.role == "user").ToList();
            var dbCourier = _context.tbl_courier.ToList();

            var data = new CourierViewModel
            {
                Order = dbOrder,
                Courier = dbCourier,
                User = dbUser
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Track(tbl_courier courier)
        {
            dynamic user_id = Session["id"];
            dynamic name = Session["name"];

            using (var save = _context.Database.BeginTransaction())
            {
                try
                {
                    if (courier.lat != null && courier.@long != null && courier.id_order != null)
                    {
                        courier.delivery_date = DateTime.Now;
                        courier.is_deleted = 0;

                        _context.tbl_courier.Add(courier);
                        _context.SaveChanges();
                        save.Commit();
                    }
                }
                catch (Exception e)
                {
                    tbl_log_history_error err = new tbl_log_history_error
                    {
                        user_name = name,
                        error_message = e.Message,
                        error_position = "Add Location Courier",
                        error_date = DateTime.Now,
                        is_deleted = 0,
                    };

                    _contextError.tbl_log_history_error.Add(err);
                    _contextError.SaveChanges();
                    save.Rollback();
                }
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        //History Tracking
        [HttpGet]
        public JsonResult GetdataTrack()
        {
            long id_courier = (long)Session["id"];
            _context.Configuration.ProxyCreationEnabled = false;

            var dbOrder = _context.tbl_order.AsNoTracking().Where(o => o.id_courier == id_courier).ToList();
            var dbUser = _context.tbl_user.Where(u => u.id == id_courier).ToList();
            var dbCourier = _context.tbl_courier.ToList();

            var data = new TrackingHistoryCourier
            {
                Order = dbOrder,
                Courier = dbCourier,
                User = dbUser
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TrackOrder()
        {
            return View();
        }

        //[HttpPost]
        //public JsonResult Track(string lat, string lng, string location)
        //{
        //    return Json(new {​​ Success = true }​​, JsonRequestBehavior.AllowGet);
        //}
    }
}