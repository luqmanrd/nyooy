﻿using Newtonsoft.Json;
using Nyooy.Models;
using Nyooy.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nyooy.Controllers
{
    [AdminSessionCheck]
    public class AdminController : Controller
    {
        private db_nyooyEntities _context;
        private db_nyooyEntities _contextError;

        public AdminController()
        {
            _context = new db_nyooyEntities();
            _contextError = new db_nyooyEntities();
        }

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public string GetData()
        {
            var dbTotalProduct = _context.tbl_product.Where(p => p.is_deleted == 0).Count().ToString();
            var dbTotalReviews = _context.tbl_review.Count().ToString();
            var dbTotalUsers = _context.tbl_user.Where(u => u.role == "user").Count().ToString();
            var dbTotalStaff = _context.tbl_user.Where(u => u.role != "user").Count().ToString();
            var dbProductSales = (from d in
                                    (from d in _context.tbl_detail_order
                                     where
                                       d.tbl_order.order_status == "Success"
                                     select new
                                     {
                                         d.qty,
                                         Dummy = "x"
                                     })
                                  group d by new { d.Dummy } into g
                                  select new
                                  {
                                      sales = (int?)g.Sum(p => p.qty)
                                  }).FirstOrDefault();

            var dbLast30days = (from d in _context.tbl_detail_order
                                where
                                  d.tbl_order.order_status == "Success" &&
                                  d.tbl_order.order_date >= SqlFunctions.DateAdd("month", -1, SqlFunctions.GetDate())
                                group new { d.tbl_order, d } by new
                                {
                                    Column1 = DbFunctions.TruncateTime(d.tbl_order.order_date)
                                } into g
                                select new
                                {
                                    Tanggal_Transaksi = g.Key.Column1,
                                    Column1 = (int?)g.Sum(p => p.d.total_price)
                                }).ToList();
            var data = new
            {
                productSales = dbProductSales.sales,
                totalProduct = dbTotalProduct,
                totalReview = dbTotalReviews,
                totalUsers = dbTotalUsers,
                totalStaff = dbTotalStaff,
                a = dbLast30days
            };
            return JsonConvert.SerializeObject(data);
        }
    }
}