﻿using Nyooy.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nyooy.Controllers
{
    [LoginCheck]
    public class ProfileController : Controller
    {
        private db_nyooyEntities _context;
        private ErrorLog _err;

        public ProfileController()
        {
            _context = new db_nyooyEntities();
            _err = new ErrorLog();
        }

        // GET: Profile
        public ActionResult Index()
        {
            dynamic user_id = Session["id"];
            dynamic name = Session["name"];

            long id = user_id;

            var dbUser = _context.tbl_user.Where(u => u.id == id).SingleOrDefault();

            return View(dbUser);
        }

        [HttpPost]
        public ActionResult Edit(tbl_user user, HttpPostedFileBase files)
        {
            dynamic name = Session["name"];

            if (user.password == null || user.confirmPassword == null)
            {
                ViewBag.Message = "Please Input Your Password!";
                return View("Index");
            }

            var password = EncryptDecrypt.Base64Encode(user.password);
            var dbUser = _context.tbl_user.Where(u => u.id == user.id).SingleOrDefault();

            using (var save = _context.Database.BeginTransaction())
            {
                try
                {
                    if (user.password != user.confirmPassword)
                    {
                        ViewBag.Message = "Password does not match!";
                        return View("Index");
                    }
                    else if (dbUser.password != password)
                    {
                        ViewBag.Message = "Password wrong!";
                        return View("Index");
                    }

                    if (user.name != null)
                    {
                        dbUser.name = user.name;
                    }

                    if (user.no_hp != null)
                    {
                        dbUser.no_hp = user.no_hp;
                    }

                    if (user.dob != null)
                    {
                        dbUser.dob = user.dob;
                    }

                    if (user.address != null)
                    {
                        dbUser.address = user.address;
                    }

                    if (files != null)
                    {
                        var ext = System.IO.Path.GetExtension(files.FileName);
                        var file_name_as_user = user.name + ext;
                        //var fileName = Path.GetFileName(files.FileName);
                        string path = Path.Combine(Server.MapPath("~/Images/photo/"), file_name_as_user);
                        user.foto = file_name_as_user;
                        dbUser.foto = user.foto;
                        files.SaveAs(path);
                    }

                    _context.SaveChanges();
                    save.Commit();

                    ViewBag.Success = "Update Profile Success!";
                    return View("Index");

                }
                catch (Exception e)
                {
                    _err.Save(name, e.Message, "Submit Form Profile");
                    save.Rollback();
                    return View("Index");
                }
            }              
        }

        public ActionResult ChangePassword()
        {
            dynamic user_id = Session["id"];
            dynamic name = Session["name"];

            long id = user_id;

            var dbUser = _context.tbl_user.Where(u => u.id == id).SingleOrDefault();

            return View(dbUser);
        }

        [HttpPost]
        public ActionResult NewPassword(tbl_user user)
        {
            dynamic name = Session["name"];

            using (var save = _context.Database.BeginTransaction())
            {
                try
                {
                    var password = EncryptDecrypt.Base64Encode(user.oldPassword);
                    var newPassword = EncryptDecrypt.Base64Encode(user.password);
                    var dbUser = _context.tbl_user.Where(u => u.id == user.id).SingleOrDefault();

                    if (dbUser.password != password)
                    {
                        ViewBag.Message = "Password Wrong!";
                        return View("ChangePassword");
                    }

                    if (user.password != user.confirmPassword)
                    {
                        ViewBag.Message = "Password does not match!";
                        return View("ChangePassword");
                    }

                    if (user.password != null)
                    {
                        dbUser.password = newPassword;
                    }

                    _context.SaveChanges();
                    save.Commit();

                    ViewBag.Success = "Update Password Success!";
                    return View("ChangePassword");

                }
                catch (Exception e)
                {
                    _err.Save(name, e.Message, "Submit Form New Password");
                    save.Rollback();
                    return View("Index");
                }
            }

        }

    }
}