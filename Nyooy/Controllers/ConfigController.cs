﻿using Nyooy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nyooy.Controllers
{
    public class ConfigController : Controller
    {
        db_nyooyEntities _context;
        private ErrorLog _err;
        public ConfigController()
        {
            _context = new db_nyooyEntities();
            _err = new ErrorLog();
        }

        [SuperAdminSessionCheck]
        // GET: Config
        public ActionResult Index()
        {
            var db = _context.tbl_config.Where(c => c.id == 1).SingleOrDefault();
            return View(db);
        }

        [HttpPost]
        public ActionResult Save(tbl_config config)
        {
            dynamic name = Session["name"];
            var db = _context.tbl_config.Where(c => c.id == config.id).SingleOrDefault();

            using (var save = _context.Database.BeginTransaction())
            {
                try
                {

                    if (db != null)
                    {
                        db.time_get_location = config.time_get_location;
                    }

                    _context.SaveChanges();
                    save.Commit();

                    ViewBag.Success = "Update Success!";

                }
                catch (Exception e)
                {
                    _err.Save(name, e.Message, "Submit Form Setting");
                    save.Rollback();
                }

                return View("Index");
            }
        }
    }
}