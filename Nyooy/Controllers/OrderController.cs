﻿using Newtonsoft.Json;
using Nyooy.Models;
using Nyooy.ViewModels;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nyooy.Controllers
{
    public class OrderController : Controller
    {
        private db_nyooyEntities _context;
        private ErrorLog _err;
        private List<CartViewModel> listOfCartModel;

        public OrderController()
        {
            _context = new db_nyooyEntities();
            _err = new ErrorLog();
            listOfCartModel = new List<CartViewModel>();
        }

        // GET: Order
        public ActionResult Index()
        {
            if (Session["id"] != null)
            {
                long user_id = Convert.ToInt32(Session["id"]);
                var db = (from d in _context.tbl_detail_order
                          join o in _context.tbl_order on d.id_order equals o.id
                          where o.id_user == user_id
                          group d by new { o.id, o.order_category, o.order_date, o.order_status, o.delivery_fee } into g
                          select new YourOrderViewModel
                          {
                              id = g.Key.id,
                              order_category = g.Key.order_category,
                              order_date = g.Key.order_date,
                              order_status = g.Key.order_status,
                              total_price = g.Sum(d => d.total_price) + g.Key.delivery_fee
                          }).OrderByDescending(o=> o.order_date).ToList();
                return View(db);
            }
            else
            {
                return this.RedirectToAction("Login", "Login");
            }

        }

        public ActionResult Detail(int id)
        {
            try
            {
                long user_id = Convert.ToInt32(Session["id"]);
                var order = _context.tbl_order.Where(p => p.id_user == user_id && p.id == id && p.is_deleted == 0).FirstOrDefault();
                ViewBag.Order = order;
                long total = 0;
                var db = (from d in _context.tbl_detail_order
                          join p in _context.tbl_product on d.id_product equals p.id
                          where d.id_order == id
                          select new DetailViewModel
                          {
                              name = p.name,
                              qty = d.qty,
                              total_price = d.total_price
                          }).ToList();
                foreach (var item in db)
                {
                    total += Convert.ToInt64(item.total_price);
                }
                DateTime orderdate = order.order_date.Value;
                DateTime maxOrderDate = orderdate.AddMinutes(30).AddSeconds(10);
                ViewBag.maxOrderDate = maxOrderDate.ToString("yyyy-MM-ddTHH:mm:ss");
                ViewBag.review = _context.tbl_review.Where(r => r.id_order == id).SingleOrDefault();
                ViewBag.sub_order = total;
                ViewBag.total_price = total + ViewBag.Order.delivery_fee;
                if (TempData["Success"] != null)
                {
                    ViewBag.Success = TempData["Success"].ToString();
                    TempData.Remove("Success");
                }
                if (TempData["Error"] != null)
                {
                    ViewBag.Error = TempData["Error"].ToString();
                    TempData.Remove("Error");
                }
                return View(db);
            }
            catch
            {
                return RedirectToAction("Index", "Order");
            }
            
        }

        public ActionResult SaveOrder(tbl_order order, tbl_detail_order detail_order)
        {
            dynamic user_id = Session["id"];
            dynamic name = Session["name"];

            using (var save = _context.Database.BeginTransaction())
            {
                try
                {
                    order.id_user = user_id;
                    order.order_category = Session["OrderType"].ToString();
                    if (Session["OrderType"].ToString() == "Event")
                    {
                        if (Session["EventName"] != null)
                        {
                            order.event_name = Session["EventName"].ToString();
                        }
                        else
                        {
                            order.event_name = "-";
                        }
                        if (Session["EventOrganizer"] != null)
                        {
                            order.organizer = Session["EventOrganizer"].ToString();
                        }
                        else
                        {
                            order.organizer = "-";
                        }
                    }

                    if (order.delivery_address == null)
                    {
                        order.delivery_address = Session["address"].ToString();
                    }

                    if(order.delivery_fee == null)
                    {
                        TempData["Error"] = "Please Wait Until Delivery Fee Appears!";
                        return this.RedirectToAction("Checkout", "Order");
                    }
                    order.order_status = "Need to Pay";
                    order.order_date = DateTime.Now;
                    order.is_deleted = 0;
                    _context.tbl_order.Add(order);
                    _context.SaveChanges();
                    long orderId = order.id;
                    listOfCartModel = Session["CartItem"] as List<CartViewModel>;
                    foreach (var item in listOfCartModel)
                    {
                        detail_order.id_order = orderId;
                        detail_order.id_product = item.id_product;
                        detail_order.qty = item.qty;
                        detail_order.total_price = item.total_price;
                        detail_order.is_deleted = 0;
                        _context.tbl_detail_order.Add(detail_order);
                        _context.SaveChanges();
                    }
                    save.Commit();
                    Session.Remove("CartItem");
                    Session.Remove("CartCounter");
                    Session.Remove("OrderType");
                    Session.Remove("EventName");
                    Session.Remove("EventOrganizer");
                    return this.RedirectToAction("Detail", "Order", new { id = orderId });
                }
                catch (Exception e)
                {
                    _err.Save(name, e.Message, "Checkout Order");
                    save.Rollback();
                    return this.RedirectToAction("Index", "Home");
                }

            }
        }

        public ActionResult SavePaymentSlip(tbl_order order, HttpPostedFileBase payment_slip)
        {
            dynamic user_id = Session["id"];
            dynamic name = Session["name"];

            using (var save = _context.Database.BeginTransaction())
            {
                try
                {
                    var data_order = _context.tbl_order.Where(p => p.id == order.id && p.is_deleted == 0).FirstOrDefault();
                    if (payment_slip != null)
                    {
                        var ext = System.IO.Path.GetExtension(payment_slip.FileName);
                        var file_name_as_payment_slip = order.id + "-payment" + ext;
                        //var fileName = Path.GetFileName(files.FileName);
                        string path = Path.Combine(Server.MapPath("~/Images/payment-slip/"), file_name_as_payment_slip);
                        order.order_payment = file_name_as_payment_slip;
                        payment_slip.SaveAs(path);
                        data_order.order_status = "Wait for Payment Confirmation";
                        data_order.order_payment = order.order_payment;
                        _context.SaveChanges();
                        save.Commit();
                        TempData["Success"] = "Payment Slip Successfully Saved!";
                        return this.RedirectToAction("Detail", "Order", new { id = order.id });
                    }
                    else
                    {
                        TempData["Error"] = "Please Select Payment Slip Image!";
                        return this.RedirectToAction("Detail", "Order", new { id = order.id });
                    }
                }
                catch (Exception e)
                {
                    _err.Save(name, e.Message, "Upload Payment Slip");
                    save.Rollback();
                    TempData["Error"] = "Upload Payment Slip Failed, Please Re-Upload!";
                    return this.RedirectToAction("Detail", "Order", new { id = order.id });
                }
            }
            
        }

        [HttpPost]
        public JsonResult ConfirmBarcodePayment(string orderId)
        {
            int id = Convert.ToInt32(orderId);
            var order = _context.tbl_order.Where(o => o.id == id).FirstOrDefault();
            order.order_status = "On Process";
            _context.SaveChanges();
            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Checkout()
        {
            if (Session["email"] == null)
            {
                Session["Checkout"] = "true";
                return RedirectToAction("Index", "Login");
            }

            if (Session["CartItem"] == null)
            {
                return RedirectToAction("Cart", "Order");
            }

            if (Session["OrderType"] == null)
            {
                Session["OrderType"] = "Personal";
            }

            ViewBag.Payment = _context.tbl_payment.Where(p => p.is_deleted == 0).ToList();
            ViewBag.CartItem = Session["CartItem"];
            int total = 0;
            int total_qty = 0;
            foreach (var item in ViewBag.CartItem)
            {
                total += item.total_price;
                total_qty += item.qty;
            }

            if (Session["OrderType"].ToString() == "Event" && total_qty < 25)
            {
                TempData["Error"] = "For Event Order, Total Product Quantity Must Be at Least 25";
                return this.RedirectToAction("Cart", "Order");
            }

            if (TempData["Error"] != null)
            {
                ViewBag.Error = TempData["Error"].ToString();
                TempData.Remove("Error");
            }

            ViewBag.TotalPrice = total;
            ViewBag.TotalQty = total_qty;
            return View();
        }

        public ActionResult Cart()
        {
            if (Session["OrderType"] == null)
            {
                Session["OrderType"] = "Personal";
            }

            if (TempData["Error"] != null)
            {
                ViewBag.Error = TempData["Error"].ToString();
                TempData.Remove("Error");
            }
            return View();
        }

        public ActionResult Personal()
        {
            Session["EventName"] = null;
            Session["EventOrganizer"] = null;
            Session["OrderType"] = "Personal";
            return RedirectToAction("Cart", "Order");
        }

        public ActionResult Event()
        {
            Session["OrderType"] = "Event";
            return RedirectToAction("Cart", "Order");
        }

        public string CartList()
        {
            listOfCartModel = Session["CartItem"] as List<CartViewModel>;
            return JsonConvert.SerializeObject(listOfCartModel);
        }

        [HttpPost]
        public JsonResult SaveEventInformation(string eventName, string eventOrganizer)
        {
            Session["EventName"] = eventName;
            Session["EventOrganizer"] = eventOrganizer;
            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string EventInformation()
        {
            var jsonData = new
            {
                eventName = Session["EventName"].ToString(),
                eventOrganizer = Session["EventOrganizer"].ToString()
            };
            return JsonConvert.SerializeObject(jsonData);
        }

        [HttpPost]
        public JsonResult UpdateQtyCart(string itemid, string qty)
        {
            int id = Convert.ToInt32(itemid);
            int quantity = Convert.ToInt32(qty);
            listOfCartModel = Session["CartItem"] as List<CartViewModel>;
            listOfCartModel.Single(model => model.id_product == id).qty = quantity;
            listOfCartModel.Single(model => model.id_product == id).total_price = quantity * listOfCartModel.Single(model => model.id_product == id).price;
            Session["CartCounter"] = listOfCartModel.Count;
            Session["CartItem"] = listOfCartModel;
            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveFromCart(long itemid)
        {
            listOfCartModel = Session["CartItem"] as List<CartViewModel>;
            listOfCartModel.RemoveAll(m => m.id_product == itemid);

            if (listOfCartModel.Count() == 0)
            {
                Session["CartCounter"] = null;
                Session["CartItem"] = null;
            }
            else
            {
                Session["CartCounter"] = listOfCartModel.Count();
                Session["CartItem"] = listOfCartModel;
            }
            return Json(new { Success = true, Counter = listOfCartModel.Count() }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public JsonResult AddToCart(long itemid)
        {
            CartViewModel objCartModel = new CartViewModel();
            var product = _context.tbl_product.Where(p => p.id == itemid).SingleOrDefault();
            if (Session["CartCounter"] != null)
            {
                listOfCartModel = Session["CartItem"] as List<CartViewModel>;
            }

            if (listOfCartModel.Any(model => model.id_product == itemid))
            {
                objCartModel = listOfCartModel.Single(model => model.id_product == itemid);
                objCartModel.qty = objCartModel.qty + 1;
                objCartModel.total_price = objCartModel.price * objCartModel.qty;

            }
            else
            {
                objCartModel.id_product = product.id;
                objCartModel.name = product.name;
                objCartModel.photo = product.photo;
                objCartModel.qty = 1;
                if (product.promo == null || product.promo == 0)
                {
                    objCartModel.price = product.price;
                }
                else
                {
                    objCartModel.price = product.promo;
                }

                objCartModel.total_price = objCartModel.price;

                listOfCartModel.Add(objCartModel);



            }
            Session["CartCounter"] = listOfCartModel.Count;
            Session["CartItem"] = listOfCartModel;
            return Json(new { Success = true, Counter = listOfCartModel.Count() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult getDistance(string latitude, string longitude)
        {
            double lat = double.Parse(latitude, CultureInfo.InvariantCulture);
            double lng = double.Parse(longitude, CultureInfo.InvariantCulture);
            GeoCoordinate geoNyooy = new GeoCoordinate(-7.9573029, 112.6503437);
            GeoCoordinate geoAddress = new GeoCoordinate(lat, lng);
            double distanceTo = geoNyooy.GetDistanceTo(geoAddress);
            distanceTo = distanceTo / 1000;
            int distanceKM = Convert.ToInt32(Math.Ceiling(distanceTo));
            int deliveryFee = distanceKM * 2000;
            string delivery_fee = string.Format("{0:C}", distanceKM * 2000);
            return Json(new { Success = true, distance = distanceKM, deliveryFee = deliveryFee, deliveryFeeString = delivery_fee, lati= lat, longi =lng }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetTrack(string orderId)
        {
            int id = Convert.ToInt32(orderId);
            _context.Configuration.ProxyCreationEnabled = false;
            var dbCourier = _context.tbl_courier.Where(c => c.id_order == id).ToList();
            return Json(dbCourier, JsonRequestBehavior.AllowGet);
        }
    }
}