﻿using Nyooy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nyooy.Controllers
{
    [SuperAdminSessionCheck]
    public class ReviewsController : Controller
    {
        private db_nyooyEntities _context;
        private db_nyooyEntities _contextError;

        public ReviewsController()
        {
            _context = new db_nyooyEntities();
            _contextError = new db_nyooyEntities();
        }

        // GET: Reviews
        public ActionResult Index()
        {
            var dbReview = _context.tbl_review.OrderByDescending(r => r.review_date).ToList();
            if (TempData["Success"] != null)
            {
                ViewBag.Success = TempData["Success"].ToString();
                TempData.Remove("Success");
            }
            if (TempData["Error"] != null)
            {
                ViewBag.Error = TempData["Error"].ToString();
                TempData.Remove("Error");
            }
            return View(dbReview);
        }

        public ActionResult Detail(int id)
        {
            var dbReview = _context.tbl_review.Where(r => r.id == id).SingleOrDefault();
            return View(dbReview);
        }

        [HttpPost]
        public ActionResult Save(tbl_review review)
        {
            dynamic name = Session["name"];

            using (var save = _context.Database.BeginTransaction())
            {
                try
                {
                    var db_update_review = _context.tbl_review.Where(r => r.id == review.id).SingleOrDefault();
                    db_update_review.review_status = review.review_status;
                    _context.SaveChanges();
                    save.Commit();
                    TempData["Success"] = "Review Data Successfully Updated!";
                    return this.RedirectToAction("Index", "Reviews");
                }
                catch (Exception e)
                {
                    tbl_log_history_error err = new tbl_log_history_error
                    {
                        user_name = name,
                        error_message = e.Message,
                        error_position = "Update Status Review",
                        error_date = DateTime.Now,
                        is_deleted = 0,
                    };

                    _contextError.tbl_log_history_error.Add(err);
                    _contextError.SaveChanges();
                    ViewBag.Message = e.Message;
                    save.Rollback();
                    TempData["Error"] = "Review Data Failed to Update!";
                    return this.RedirectToAction("Index", "Reviews");
                }
            }
        }
    }
}