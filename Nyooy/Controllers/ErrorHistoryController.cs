﻿using Nyooy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nyooy.Controllers
{
    [SuperAdminSessionCheck]
    public class ErrorHistoryController : Controller
    {
        private db_nyooyEntities _context;
        private db_nyooyEntities _contextError;

        public ErrorHistoryController()
        {
            _context = new db_nyooyEntities();
            _contextError = new db_nyooyEntities();
        }
        // GET: ErrorHistory
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Getdata()
        {
            long id_courier = (long)Session["id"];
            _context.Configuration.ProxyCreationEnabled = false;

            //NOTE : STATUS ORDER SHOULD BE 'ON DELIVERY'
            var dbErrorLog = _context.tbl_log_history_error.ToList();

            return Json(dbErrorLog, JsonRequestBehavior.AllowGet);
        }
    }
}