﻿using Nyooy.Models;
using Nyooy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nyooy.Controllers
{
    public class HomeController : Controller
    {
        private db_nyooyEntities _context;
        
        public HomeController()
        {
            _context = new db_nyooyEntities();
        }

        public ActionResult Index()
        {
            //GET MAEM'E ITEMS
            ViewBag.maem = _context.tbl_product.Where(p => p.category == "Maem'e" && p.is_deleted == 0).Take(4).ToList();
            ViewBag.promo = _context.tbl_product.Where(p => p.category == "Promo" && p.is_deleted == 0).Take(2).ToList();

            var dbReview = _context.tbl_review.Where(r => r.review_status == 1).ToList();
            var dbOrder = _context.tbl_order.ToList();
            var dbUser = _context.tbl_user.ToList();

            var viewModel = new ReviewViewModel
            {
                Review = dbReview,
                Order = dbOrder,
                User = dbUser
            };

            return View(viewModel);
        }

        public ActionResult Promo()
        {
            var db = _context.tbl_product.Where(p => p.category == "Promo").Where(p => p.is_deleted == 0).ToList();
            return View(db);
        }

        public ActionResult About()
        {
            return View();
        }
        
    }
}