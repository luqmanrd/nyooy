﻿using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using Nyooy.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Nyooy.Controllers
{
    [AdminSessionCheck]
    public class ReportController : Controller
    {
        ReportViewer report;
        ReportParameter[] reportParameters;

        dynamic ssrsUrl;
        dynamic date;

        public ReportController()
        {
            ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            report = ComponentReport(ssrsUrl);
            date = DateTime.Now.ToString("dd MMMM yyyy");
        }
        // GET: Report
        public ActionResult Index()
        {
            //Transaksi Report
            ViewBag.ReportViewer = pathReport("/TransaksiReport", Parameters());

            return View();
        }

        public ActionResult ProductReport()
        {
            ViewBag.ReportViewer = pathReport("/ProductReport", Parameters());

            return View();
        }

        public ActionResult CourierReport()
        {
            ViewBag.ReportViewer = pathReport("/CourierReport", null);

            return View();
        }

        public ActionResult UserGrowthReport()
        {
            ViewBag.ReportViewer = pathReport("/UserGrowthReport", Parameters());

            return View();
        }

        public ActionResult ReviewReport()
        {
            ViewBag.ReportViewer = pathReport("/ReviewReport", Parameters());

            return View();
        }

        public ReportParameter[] Parameters()
        {
            reportParameters = new ReportParameter[2];
            reportParameters[0] = new ReportParameter("StartDate", date, true);
            reportParameters[1] = new ReportParameter("EndDate", date, true);

            return reportParameters;
        }

        public ReportViewer ComponentReport(dynamic ssrs)
        {
            report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.SizeToReportContent = true;
            report.Width = Unit.Percentage(100);
            report.Height = Unit.Percentage(100);
            report.ServerReport.ReportServerUrl = new Uri(ssrs);

            return report;
        }

        public ReportViewer pathReport(string path, ReportParameter[] parameter)
        {
            report.ServerReport.ReportPath = path;
            if (parameter != null)
            {
                report.ServerReport.SetParameters(parameter);
            }
            report.ServerReport.Refresh();

            return report;
        }

    }
}