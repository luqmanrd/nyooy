﻿using Nyooy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nyooy.Controllers
{
    public class ReviewController : Controller
    {
        private db_nyooyEntities _context;
        private ErrorLog _err;

        public ReviewController()
        {
            _context = new db_nyooyEntities();
            _err = new ErrorLog();
        }
        // GET: Review
        public ActionResult Index()
        {
            dynamic user_id = Session["id"];

            if (user_id == null)
            {
                return RedirectToAction("Login", "Login");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Save(tbl_review review)
        {
            long user_id = (long)Session["id"];
            dynamic name = Session["name"];

            using (var save = _context.Database.BeginTransaction())
            {
                try
                {
                    var dbOrder = _context.tbl_order.Where(o => o.id == review.id_order && o.id_user == user_id && o.order_status == "Success").SingleOrDefault();
                    var dbReview = _context.tbl_review.Where(r => r.id_order == review.id_order).SingleOrDefault();

                    //if (dbOrder == null)
                    //{
                    //    ViewBag.Message = "Order Number Not Found!";
                    //    return View("Index");
                    //}
                    //else if (dbReview != null)
                    //{
                    //    ViewBag.Message = "Order Number has been review";
                    //    return View("Index");
                    //}

                    review.review_date = DateTime.Now;
                    review.review_status = 0;
                    review.is_deleted = 0;

                    _context.tbl_review.Add(review);
                    _context.SaveChanges();
                    save.Commit();
                    //ViewBag.Success = "Update Review Success!";
                    //return View("Index");
                    return this.RedirectToAction("Detail", "Order", new { id = review.id_order });
                }
                catch (Exception e)
                {
                    _err.Save(name, e.Message, "Submit Form Review");
                    save.Rollback();
                    return this.RedirectToAction("Detail", "Order", new { id = review.id_order });
                }
                
            }
        }
    }
}