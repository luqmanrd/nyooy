﻿using Nyooy.Models;
using Nyooy.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Nyooy.Controllers
{
    [SuperAdminSessionCheck]
    public class UsersController : Controller
    {
        private db_nyooyEntities _context;
        private ErrorLog _err;

        public UsersController()
        {
            _context = new db_nyooyEntities();
            _err = new ErrorLog();
        }

        // GET: Users
        public ActionResult Index()
        {
            var db = _context.tbl_user.Where(p => p.is_deleted == 0).OrderBy(p => p.role).ToList();
            return View(db);
        }

        public ActionResult Details(int id)
        {
            var db = _context.tbl_user.Where(p => p.id == id).SingleOrDefault();

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Super Admin", Value = "super_admin" });
            items.Add(new SelectListItem { Text = "Admin", Value = "admin" });
            items.Add(new SelectListItem { Text = "User", Value = "user" });
            ViewBag.Departments = items;

            return View(db);
        }

        public ActionResult Save(tbl_user user)
        {
            dynamic user_id = Session["id"];
            dynamic name = Session["name"];
            using (var save = _context.Database.BeginTransaction())
            {
                try
                {
                    var db_edit_product = _context.tbl_user.Where(p => p.id == user.id).SingleOrDefault();
                    db_edit_product.role = user.role;
                    _context.SaveChanges();
                    save.Commit();

                    //Send email order_status
                    var dbUser = _context.tbl_user.Where(u => u.id == user.id).SingleOrDefault();

                    //Declare variable
                    var emailFrom = "nyooy.id@gmail.com"; //set Email Address FROM
                    var password = "GOFOOD & GRAB FOOD"; //set Password
                    var emailTo = dbUser.email; //set Email Address TO

                    var subject = "Your Role";
                    var title = "Nyooy.id";
                    var content = @"User Notification: “Your ROLE is <strong>" + user.role + "</strong> Now!”";
                    var footer = "Thank your for your order!";

                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                    mail.From = new MailAddress(emailFrom);
                    mail.To.Add(emailTo);
                    mail.Subject = subject;
                    //mail.Body = "This is for testing SMTP mail from GMAIL";
                    mail.Body = BodyHtml(title, content, footer);
                    mail.IsBodyHtml = true;

                    SmtpServer.Port = 587; //-> for Gmail use port 587 
                    SmtpServer.Credentials = new System.Net.NetworkCredential(emailFrom, password);
                    SmtpServer.EnableSsl = true;
                    SmtpServer.Send(mail);

                    return RedirectToRoute(new { controller = "Users", action = "Index" });
                }
                catch (Exception e)
                {
                    _err.Save(name, e.Message, "Manage Users");
                    save.Rollback();
                    return RedirectToRoute(new { controller = "Users", action = "Index" });
                }
            }
        }

        public string BodyHtml(string title, string bodyContent, string footer)
        {
            var bodyHtml = @"<html>" +
            "<body>" +
            "<table bgcolor='#f5feff'  align='center' border='0' cellpadding='0' cellspacing='0' width='80%' style='border: none;'>" +
                "<tr>" +
                    "<td align='center' style='padding: 0 0 0 0;'>" +
                        "<img src='https://miro.medium.com/max/712/1*c3cQvYJrVezv_Az0CoDcbA.jpeg' width='100%' alt='Notification' style='display: block;' />" +
                    "</td>" +
                "</tr>" +
                "<tr>" +
                    "<td style='padding: 40px 30px 40px 30px;'>" +
                        "<table border='0' cellpadding='0' cellspacing='0' width='100%'>" +
                            "<tr style='text-align:center; color: #153643; font-family: Arial, sans-serif; font-size: 24px;'>" +
                                "<td>" +
                                    "<strong>" + title + "</strong>" +
                                "</td>" +
                            "</tr>" +
                            "<tr style='color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;'>" +
                                "<td style='padding: 20px 0 30px 0; text-align: justify;'>" +
                                    bodyContent +
                                "</td>" +
                            "</tr>" +
                        "</table>" +
                    "</td>" +
                "</tr>" +
                "<tr style='text-align:center'>" +
                    "<td bgcolor='#ffe05d' style='padding: 30px 30px 30px 30px;'>" +
                        footer +
                    "</td>" +
                "</tr>" +
                "</table>" +
                "</body>" +
                "</html>";

            return bodyHtml;
        }

        public ActionResult Delete(long id)
        {
            var user_delete = _context.tbl_user.Where(p => p.id == id).SingleOrDefault();
            user_delete.is_deleted = 1;
            _context.SaveChanges();
            return RedirectToRoute(new { controller = "Users", action = "Index" });
        }

    }
}