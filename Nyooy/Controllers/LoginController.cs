﻿using Nyooy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Net.Mail;
using System.Net;
using System.ComponentModel.DataAnnotations;

namespace Nyooy.Controllers
{
    
    public class LoginController : Controller
    {
        private db_nyooyEntities _context;
        private ErrorLog _err;
        private db_nyooyEntities _contextTokenActivation;

        //EncryptDecrypt secure;

        public LoginController()
        {
            _context = new db_nyooyEntities();
            _err = new ErrorLog();
            _contextTokenActivation = new db_nyooyEntities();
            //secure = new EncryptDecrypt();
        }

        // GET: Login
        [Route("login")]
        [SessionCheck]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [SessionCheck]
        public ActionResult CheckLogin(tbl_user user)
        {
            try
            {
                if (user.email == null || user.password == null)
                {
                    ViewBag.Message = "All Fields are Required!";
                    return View("Login");
                }
                else
                {
                    var encrypt = EncryptDecrypt.Base64Encode(user.password);

                    var obj = _context.tbl_user.Where(a => a.email.Equals(user.email) && a.password.Equals(encrypt)).FirstOrDefault();
                    if (obj != null)
                    {

                        if (obj.email_active == 0 || obj.email_active == null && obj.role == "user")
                        {
                            ViewBag.Message = "Please Confirm Your Email!";
                            return View("Login");
                        }

                        Session["id"] = obj.id;
                        Session["email"] = obj.email.ToString();
                        Session["name"] = obj.name.ToString();
                        Session["role"] = obj.role.ToString();
                        Session["phone"] = obj.no_hp.ToString();
                        Session["foto"] = obj.foto;
                        Session["address"] = obj.address.ToString();
                        if (obj.role.ToString() == "admin" || obj.role.ToString() == "super_admin")
                        {
                            //Redirect to admin page
                            return this.RedirectToAction("Index", "Admin");
                        }
                        else
                        {
                            //Redirect to home page
                            if (Session["Checkout"] != null)
                            {
                                Session["Checkout"] = null;
                                return this.RedirectToAction("Checkout", "Order");
                            }
                            else
                            {
                                return this.RedirectToAction("Index", "Home");
                            }
                            
                        }
                    }
                    else
                    {
                        ViewBag.Message = "Wrong Email or Password! Please Try Again.";
                        return View("Login");
                    }
                }

            }
            catch (Exception e)
            {
                dynamic id = Convert.ToInt32(user.id);

                if (id != null)
                {
                    id = user.id;
                }
                else
                {
                    id = 0;
                }

                _err.Save("visitor", e.Message, "Form Login");
                ViewBag.Message = e.Message;
                return View("Login");
            }

        }

        [Route("register")]
        [SessionCheck]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(tbl_user user)
        {
            using(var registration = _context.Database.BeginTransaction())
            {
                try
                {
                    user.role = "user";
                    user.is_deleted = 0;
                    user.register_date = DateTime.Now;

                    var obj = _context.tbl_user.Where(u => u.email == user.email).ToList();

                    if (user.name == null || user.no_hp == null || user.address == null || user.dob == null || user.email == null || user.password == null)
                    {
                        ViewBag.Message = "Another Fields is Empty";
                        return View("Register");
                    }

                    if (!string.IsNullOrEmpty(user.email) && new EmailAddressAttribute().IsValid(user.email))
                    {
                    }
                    else
                    {
                        ViewBag.Message = "Please Use Valid Email Address!";
                        return View("Register");
                    }
                        

                    if (obj.Count > 0)
                    {
                        ViewBag.Message = "Email already exist";
                        return View("Register");
                    }

                    if (user.password != user.confirmPassword)
                    {
                        ViewBag.Message = "Password does not match!";
                        return View("Register");
                    }

                    var encrypt = EncryptDecrypt.Base64Encode(user.password);

                    user.password = encrypt;
                    user.email_active = 0;

                    _context.tbl_user.Add(user);
                    _context.SaveChanges();
                    ViewBag.Message = null;

                    //Activation Code
                    SendActivationEmail(user);
                    //End Activation Code
                    registration.Commit();


                    TempData["Name"] = user.name;
                    return RedirectToAction("SuccessRegistration");

                }
                catch (Exception e)
                {
                    _err.Save("visitor", e.Message, "Submit Form Registration");
                    registration.Rollback();
                    ViewBag.Success = null;
                    ViewBag.Message = e.Message;
                    return View("Register");
                }
            }

        }

        [HttpPost]
        private void SendActivationEmail(tbl_user user)
        {
            Guid activationCode = Guid.NewGuid();
            var userActivation = new tbl_user_activation()
            {
                id_user = user.id,
                activation_code = activationCode
            };
            _context.tbl_user_activation.Add(userActivation);
            _context.SaveChanges();

            var emailFrom = "nyooy.id@gmail.com"; //set Email Address FROM
            var password = "GOFOOD & GRAB FOOD"; //set Password

            using (MailMessage mm = new MailMessage(emailFrom, user.email))
            {
                var urlActivationCode = string.Format("{0}://{1}/Login/Activation/{2}", Request.Url.Scheme, Request.Url.Authority, activationCode);
                var urlContact = string.Format("{0}://{1}//Home/About", Request.Url.Scheme, Request.Url.Authority);

                mm.Subject = "Account Activation";
                //string body = "Hello " + user.name + ",";
                //body += "<br /><br />Please click the following link to activate your account";
                //body += "<br /><a href = '" + string.Format("{0}://{1}/Login/Activation/{2}", Request.Url.Scheme, Request.Url.Authority, activationCode) + "'>Click here to activate your account.</a>";
                //body += "<br /><br />Thanks";
                mm.Body = ContentBody(user.name, urlActivationCode, urlContact);
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential(emailFrom, password);
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }

        public ActionResult Activation()
        {
            ViewBag.Message = "Invalid Activation code.";
            if (RouteData.Values["id"] != null)
            {
                Guid activationCode;
                try
                {
                    activationCode = new Guid(RouteData.Values["id"].ToString());
                }
                catch (Exception e)
                {
                    _err.Save("visitor", e.Message, "Activation Code");
                    return View();
                }
                var userActivation = _context.tbl_user_activation.Where(c => c.activation_code == activationCode).FirstOrDefault();
                if (userActivation != null)
                {
                    var updateUser = _context.tbl_user.Where(u => u.id == userActivation.id_user).FirstOrDefault();

                    updateUser.email_active = 1;
                    _context.tbl_user_activation.Remove(userActivation);

                    _context.SaveChanges();
                    ViewBag.Message = "Activation successful.";
                }
            }

            return View();
        }

        [Route("logout")]
        public ActionResult Logout()
        {
            Session.Abandon();

            Session.Clear();
            Session.RemoveAll();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult SuccessRegistration()
        {
            var name = Convert.ToString(TempData["Name"]);
            if (name != "")
            {
                ViewBag.Name = Convert.ToString(TempData["Name"]);
            }
            //TempData.Clear();
            return View();
        }



        public string ContentBody(string name, string urlConfirmAccount, string urlContact)
        {
            var bodyHtml = @"<html>
                            <body>
                                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                    <!-- LOGO -->
                                    <tr>
                                        <td bgcolor='#FFA73B' width='100%' align='center' style='padding: 40px 10px 40px 10px;'></td>
                                    </tr>
                                    <tr>
                                        <td bgcolor='#FFA73B' align='center' style='padding: 0px 10px 0px 10px;'>
                                            <table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>
                                                <tr>
                                                    <td bgcolor='#ffffff' align='center' valign='top' style='padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;'>
                                                        <h1 style='font-size: 48px; font-weight: 400; margin: 2;'>Hello " + name + @"</h1> 
                                                        <img src='https://img.icons8.com/clouds/100/000000/handshake.png' width='125' height='120' style='display: block; border: 0px;' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td bgcolor='#f4f4f4' align='center' style='padding: 0px 10px 0px 10px;'>
                                            <table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>
                                                <tr>
                                                    <td bgcolor='#ffffff' align='center' style='padding: 20px 30px 40px 30px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>
                                                        <p style='margin: 0;'>We're excited to have you get started. First, you need to confirm your account. Just press the button below.</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#ffffff' align='left'>
                                                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                                                            <tr>
                                                                <td bgcolor='#ffffff' align='center' style='padding: 20px 30px 60px 30px;'>
                                                                    <table border='0' cellspacing='0' cellpadding='0'>
                                                                        <tr>
                                                                            <td align='center' style='border-radius: 3px;' bgcolor='#FFA73B'>
                                                                                <a href='" + urlConfirmAccount + @"' target='_blank' style='font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; padding: 15px 25px; border-radius: 2px; border: 1px solid #FFA73B; display: inline-block;'>Confirm Account</a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr> <!-- COPY -->
                                                <tr>
                                                    <td bgcolor='#ffffff' align='left' style='padding: 0px 30px 20px 30px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>
                                                        <p style='margin: 0;'>If you have any questions, just reply to this email—we're always happy to help out.</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td bgcolor='#ffffff' align='right' style='padding: 0px 30px 40px 30px; border-radius: 0px 0px 4px 4px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>
                                                        <p style='margin: 0;'>Cheers,<br><br><br>Nyooys Team</p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td bgcolor='#f4f4f4' align='center' style='padding: 0px 10px 40px 10px;'>
                                            <table border='0' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px;'>
                                                <tr>
                                                    <td bgcolor='#FFECD1' align='center' style='padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #666666; font-family: Lato, Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;'>
                                                        <h2 style='font-size: 20px; font-weight: 400; color: #111111; margin: 0;'>Need more help?</h2>
                                                        <p style='margin: 0;'>
                                                            <a href='" + urlContact + @"' target='_blank' style='color: #FFA73B;'>We&rsquo;re here to help you out</a>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </body>
                            </html>";

            return bodyHtml;
        }

    }
}